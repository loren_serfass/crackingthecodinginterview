package ch08_recursion_and_dynamic_programming;

public class Pair {
	public int x;
	public int y;
	
	public Pair(int x, int y) {
		this.x = x;
		this.y = y;
	}

}
