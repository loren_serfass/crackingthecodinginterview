package ch03_stacks_and_queues;

public class Node<T> {
	Node next;
	T value;

	public Node(T value) {
		next = null;
		this.value = value;
	}
}

